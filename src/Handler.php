<?php

declare(strict_types=1);

namespace Drupal\jsonrpc;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResultReasonInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\jsonrpc\Exception\ErrorHandler;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\JsonRpcObject\Error;
use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\JsonRpcObject\Request;
use Drupal\jsonrpc\JsonRpcObject\Response;

/**
 * Manages all the JSON-RPC business logic.
 */
class Handler implements HandlerInterface {

  /**
   * The support JSON-RPC version.
   *
   * @var string
   */
  const SUPPORTED_VERSION = '2.0';

  public function __construct(
    protected PluginManagerInterface $methodManager,
    protected RendererInterface $renderer,
    protected ErrorHandler $errorHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function supportedVersion(): string {
    return static::SUPPORTED_VERSION;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function batch(array $requests): array {
    return array_filter(array_map(fn(Request $request): ?Response => $this->doRequest($request), $requests));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function supportedMethods(): array {
    return $this->methodManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function supportsMethod($name): bool {
    return !is_null($this->getMethod($name));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function availableMethods(?AccountInterface $account = NULL): array {
    return array_filter($this->supportedMethods(), fn(MethodInterface $method) => $method->access('execute'));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getMethod($name): ?MethodInterface {
    return $this->methodManager->getDefinition($name, FALSE);
  }

  /**
   * Executes an RPC call and returns a JSON-RPC response.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Request $request
   *   The JSON-RPC request.
   *
   * @return \Drupal\jsonrpc\JsonRpcObject\Response|null
   *   The JSON-RPC response.
   */
  protected function doRequest(Request $request): ?Response {
    // Helper closure to handle eventual exceptions.
    $error_handler = $this->errorHandler;
    $handle_exception = function ($e, Request $request) use ($error_handler): Response {
      if (!$e instanceof JsonRpcException) {
        $id = $request->isNotification() ? FALSE : $request->id();
        $e = JsonRpcException::fromPrevious($e, $id);
      }
      $error_handler->logServerError($e);
      return $e->getResponse();
    };
    try {
      $context = new RenderContext();
      $result = $this->renderer->executeInRenderContext($context, fn(): Response|string|int|float|bool|array|null => $this->doExecution($request));

      if ($request->isNotification()) {
        return NULL;
      }
      $rpc_response = $result instanceof Response
        ? $result
        : new Response(static::SUPPORTED_VERSION, $request->id(), $result);
      $methodPluginClass = $this->getMethod($request->getMethod())->getClass();
      $result_schema = call_user_func([$methodPluginClass, 'outputSchema']);
      $rpc_response->setResultSchema($result_schema);
      $response_headers = $this->getMethod($request->getMethod())->responseHeaders;
      $rpc_response->getHeaders()->add($response_headers);

      if (!$context->isEmpty()) {
        /** @var \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata */
        $bubbleable_metadata = $context->pop();
        $rpc_response->addCacheableDependency($bubbleable_metadata);
        if ($rpc_response instanceof AttachmentsInterface) {
          $rpc_response->addAttachments($bubbleable_metadata->getAttachments());
        }
      }

      return $rpc_response;
    }
    // Catching Throwable allows us to recover from more kinds of exceptions
    // that might occur in badly written 3rd party code.
    catch (\Throwable $e) {
      return $handle_exception($e, $request);
    }
  }

  /**
   * Gets an anonymous function which executes the RPC method.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Request $request
   *   The JSON-RPC request.
   *
   * @return \Drupal\jsonrpc\JsonRpcObject\Response|array|string|int|float|bool|null
   *   The JSON-RPC output.
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  protected function doExecution(Request $request): Response|array|string|int|float|bool|null {
    if (($method = $this->getMethod($request->getMethod())) instanceof MethodInterface) {
      $this->checkAccess($method);
      $configuration = [HandlerInterface::JSONRPC_REQUEST_KEY => $request];
      $executable = $this->getExecutable($method, $configuration);
      return $request->hasParams()
        ? $executable->execute($request->getParams())
        : $executable->execute(new ParameterBag([]));
    }
    else {
      throw JsonRpcException::fromError(Error::methodNotFound($method->id()));
    }
  }

  /**
   * Gets an executable instance of an RPC method.
   *
   * @param \Drupal\jsonrpc\MethodInterface $method
   *   The method definition.
   * @param array $configuration
   *   Method configuration.
   *
   * @return object
   *   The executable method.
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   *   In case of error.
   */
  protected function getExecutable(MethodInterface $method, array $configuration): object {
    try {
      return $this->methodManager->createInstance($method->id(), $configuration);
    }
    catch (PluginException) {
      throw JsonRpcException::fromError(Error::methodNotFound($method->id()));
    }
  }

  /**
   * Check execution access.
   *
   * @param \Drupal\jsonrpc\MethodInterface $method
   *   The method for which to check access.
   *
   * @throws \Drupal\jsonrpc\Exception\JsonRpcException
   */
  protected function checkAccess(MethodInterface $method): void {
    // @todo Add cacheability metadata here.
    $access_result = $method->access('execute', NULL, TRUE);
    if (!$access_result->isAllowed()) {
      $reason = 'Access Denied';
      if ($access_result instanceof AccessResultReasonInterface && ($detail = $access_result->getReason())) {
        $reason .= ': ' . $detail;
      }
      throw JsonRpcException::fromError(Error::invalidRequest($reason, $access_result));
    }
  }

}
