<?php

declare(strict_types=1);

namespace Drupal\jsonrpc;

use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Interface for the method plugins.
 */
interface MethodInterface extends AccessibleInterface, PluginDefinitionInterface {

  /**
   * The class method to call.
   *
   * @return string
   *   The PHP method on the RPC method object to call. Defaults to: execute.
   */
  public function call(): string;

  /**
   * How to use this method.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The usage text for the method.
   */
  public function getUsage(): TranslatableMarkup;

  /**
   * The parameters for this method.
   *
   * Can be a keyed array where the parameter names are the keys or an indexed
   * array for positional parameters.
   *
   * @return \Drupal\jsonrpc\Annotation\JsonRpcParameterDefinition[]|null
   *   The method params or NULL if none are accepted.
   */
  public function getParams(): ?array;

  /**
   * Whether the parameters are by-position.
   *
   * @return bool
   *   True if the parameters are positional.
   */
  public function areParamsPositional(): bool;

}
