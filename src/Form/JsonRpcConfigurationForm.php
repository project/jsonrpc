<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\jsonrpc\Enum\JsonRpcSetting;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures the JSON-RPC module.
 */
final class JsonRpcConfigurationForm extends ConfigFormBase {

  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    protected ModuleHandlerInterface $moduleHandler,
    protected RouteBuilderInterface $routeBuilder,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): JsonRpcConfigurationForm {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('router.builder')
    );
  }

  const string FORM_ID = 'jsonrpc.settings';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [self::FORM_ID];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::FORM_ID);

    $form[self::FORM_ID] = [
      '#type' => 'details',
      '#title' => $this->t('Configure JSON-RPC access'),
      '#open' => TRUE,
    ];

    $form[self::FORM_ID][JsonRpcSetting::BasicAuth->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow access via basic authentication'),
      '#default_value' => $config->get(JsonRpcSetting::BasicAuth->value),
      '#required' => FALSE,
    ];

    $form[self::FORM_ID][JsonRpcSetting::Cookie->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow access via cookies'),
      '#default_value' => $config->get(JsonRpcSetting::Cookie->value),
      '#required' => FALSE,
    ];

    $form[self::FORM_ID][JsonRpcSetting::JWT->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow access via JWT'),
      '#default_value' => $config->get(JsonRpcSetting::JWT->value),
      '#required' => FALSE,
    ];

    $form[self::FORM_ID][JsonRpcSetting::OAuth2->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow access via basic authentication'),
      '#default_value' => $config->get(JsonRpcSetting::OAuth2->value),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(self::FORM_ID);
    $config
      ->set(JsonRpcSetting::BasicAuth->value, boolval($form_state->getValue(JsonRpcSetting::BasicAuth->value)))
      ->set(JsonRpcSetting::Cookie->value, boolval($form_state->getValue(JsonRpcSetting::Cookie->value)))
      ->set(JsonRpcSetting::JWT->value, boolval($form_state->getValue(JsonRpcSetting::JWT->value)))
      ->set(JsonRpcSetting::OAuth2->value, boolval($form_state->getValue(JsonRpcSetting::OAuth2->value)))
      ->save();

    // Altering routes requires us to rebuild the routing table.
    $this->routeBuilder->setRebuildNeeded();

    parent::submitForm($form, $form_state);
  }

}
