<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Annotation;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\jsonrpc\ParameterDefinitionInterface;
use Drupal\jsonrpc\ParameterFactory\RawParameterFactory;

/**
 * Defines a JsonRpcParameterDefinition annotation object.
 *
 * @see \Drupal\jsonrpc\Plugin\JsonRpcServiceManager
 * @see plugin_api
 *
 * @Annotation
 */
class JsonRpcParameterDefinition implements ParameterDefinitionInterface {

  /**
   * The name of the parameter if the params are by-name, an offset otherwise.
   *
   * @var string|int|null
   */
  protected string|int|null $id = NULL;

  /**
   * The parameter schema.
   *
   * Note that while annotation syntax looks similar to JSON, and in some simple
   * cases is even compatible, the schema must be written as an annotation
   * object that will then be converted to a PHP array acceptable to
   * json_encode(). Basically, this means writing arrays as {"one", "two"}
   * instead of the JSON ["one", "two"].
   *
   * WARNING: Below, the PHPDoc type does not match the PHP type.
   * This is because the PHPDoc type is used by Doctrine/Shaper for processing,
   * while the PHP type is used by PHP itself to restrict the possible types.
   *
   * @var array
   */
  public ?array $schema = NULL;

  /**
   * A description of the parameter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public ?Translation $description = NULL;

  /**
   * The parameter factory.
   *
   * WARNING: Below, the PHPDoc type does not match the PHP type.
   * This is because the PHPDoc type is used by Doctrine/Shaper for processing,
   * while the PHP type is used by PHP itself to restrict the possible types.
   *
   * @var string
   */
  public ?string $factory = NULL;

  /**
   * Whether the parameter is required.
   *
   * @var bool
   */
  public bool $required;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFactory(): string {
    return $this->factory ?? RawParameterFactory::class;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getDescription(): TranslatableMarkup {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function isRequired(): bool {
    return $this->required ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getSchema(): array {
    if ($this->schema === NULL && $this->factory !== NULL) {
      $this->schema = call_user_func_array([$this->factory, 'schema'], [$this]);
    }
    return $this->schema;
  }

  /**
   * Returns the instance.
   */
  public function get(): JsonRpcParameterDefinition {
    return $this;
  }

  /**
   * Sets the parameter ID.
   *
   * @param string|int $id
   *   The ID to set.
   */
  public function setId(string|int $id): void {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getId(): string|int {
    return $this->id;
  }

}
