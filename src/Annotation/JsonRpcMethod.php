<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Annotation;

use Drupal\Component\Annotation\AnnotationBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\jsonrpc\MethodInterface;

/**
 * Defines a JsonRpcParameterDefinition annotation object.
 *
 * @see \Drupal\jsonrpc\Plugin\JsonRpcServiceManager
 * @see plugin_api
 *
 * @Annotation
 */
class JsonRpcMethod extends AnnotationBase implements MethodInterface {

  /**
   * The access required to use this RPC method.
   *
   * @var mixed
   */
  public mixed $access = [];

  /**
   * The class method to call.
   *
   * Optional. If the method ID is 'foo.bar', this defaults to 'bar'. If the
   * method ID does not contain a dot (.), defaults to 'execute'.
   *
   * @var string|null
   */
  public ?string $call = NULL;

  /**
   * A copy of the output schema from the JSON-RPC method.
   *
   * @var null|array
   */
  public ?array $output = NULL;

  /**
   * How to use this method.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $usage;

  /**
   * The parameters for this method.
   *
   * Can be a keyed array where the parameter names are the keys or an indexed
   * array for positional parameters.
   *
   * @var \Drupal\jsonrpc\Annotation\JsonRpcParameterDefinition[]
   */
  public array $params = [];

  /**
   * Key value pairs for setting headers in the response.
   *
   * Batch requests will only contain the headers appearing on **all** of the
   * responses involved.
   *
   * @var array
   */
  public array $responseHeaders = [];

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function id(): string {
    return $this->getId();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function call(): string {
    if ($this->call === NULL) {
      $this->call = 'execute';
    }
    return $this->call;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getUsage(): TranslatableMarkup {
    return $this->usage;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getParams(): ?array {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function areParamsPositional(): bool {
    return array_reduce(array_keys($this->getParams()), fn($positional, $key): bool|array|null|int|float|string => $positional ? !is_string($key) : $positional, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function get(): JsonRpcMethod {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function access($operation = 'execute', ?AccountInterface $account = NULL, $return_as_object = FALSE): AccessResultInterface|bool {
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes, use dependency injection instead
    $account = $account ?: \Drupal::currentUser();
    switch ($operation) {
      case 'execute':
        if (is_callable($this->access)) {
          return call_user_func_array($this->access, [
            $operation,
            $account,
            $return_as_object,
          ]);
        }
        $access_result = AccessResult::allowed();
        foreach ($this->access as $permission) {
          $access_result = $access_result->andIf(AccessResult::allowedIfHasPermission($account, $permission));
        }
        break;

      case 'view':
        $access_result = $this->access('execute', $account, $return_as_object);
        break;

      default:
        $access_result = AccessResult::neutral();
        break;
    }
    return $return_as_object ? $access_result : $access_result->isAllowed();
  }

}
