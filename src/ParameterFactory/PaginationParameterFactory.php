<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\ParameterFactory;

use Drupal\jsonrpc\ParameterDefinitionInterface;
use Shaper\Util\Context;
use Shaper\Validator\ValidateableInterface;

/**
 * A parameter factory to handle paginated responses.
 */
class PaginationParameterFactory extends ParameterFactoryBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function schema(ParameterDefinitionInterface $parameter_definition): array {
    return [
      'type' => 'object',
      'properties' => [
        'limit' => [
          'type' => 'integer',
          'minimum' => 0,
        ],
        'offset' => [
          'type' => 'integer',
          'minimum' => 0,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOutputValidator(): ValidateableInterface|null {
    // The input is the same as the output.
    return $this->getInputValidator();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function doTransform($data, ?Context $context = NULL) {
    return $data;
  }

}
