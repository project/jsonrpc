<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\ParameterFactory;

use Drupal\jsonrpc\ParameterDefinitionInterface;
use Shaper\Util\Context;
use Shaper\Validator\ValidateableInterface;

/**
 * Class RawParameterFactory just returns the raw parameter.
 *
 * @package Drupal\jsonrpc\ParameterFactory
 */
class RawParameterFactory extends ParameterFactoryBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function schema(ParameterDefinitionInterface $parameter_definition): array {
    return $parameter_definition->getSchema();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOutputValidator(): ?ValidateableInterface {
    // The input is the same as the output.
    return $this->getInputValidator();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function doTransform($data, ?Context $context = NULL) {
    return $data;
  }

}
