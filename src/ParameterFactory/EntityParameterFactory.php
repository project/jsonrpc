<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\ParameterFactory;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\JsonRpcObject\Error;
use Drupal\jsonrpc\ParameterDefinitionInterface;
use JsonSchema\Validator;
use Shaper\Util\Context;
use Shaper\Validator\InstanceofValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A factory to create loaded entities from entity type & UUID user input.
 */
class EntityParameterFactory extends ParameterFactoryBase {

  /**
   * Constructs an EntityParameterFactory object.
   *
   * @param \Drupal\jsonrpc\ParameterDefinitionInterface $definition
   *   The parameter definition.
   * @param \JsonSchema\Validator $validator
   *   The validator to ensure the user input is valid.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity type repository to load entities by UUID.
   */
  public function __construct(
    ParameterDefinitionInterface $definition,
    Validator $validator,
    protected EntityRepositoryInterface $entityRepository,
  ) {
    parent::__construct($definition, $validator);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ParameterDefinitionInterface $definition, ContainerInterface $container): static {
    return new static(
      $definition,
      $container->get('jsonrpc.schema_validator'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function schema(?ParameterDefinitionInterface $parameter_definition = NULL): array {
    return [
      'type' => 'object',
      'properties' => [
        'type' => ['type' => 'string'],
        'uuid' => ['type' => 'string'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOutputValidator(): InstanceofValidator {
    return new InstanceofValidator(EntityInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function doTransform($data, ?Context $context = NULL) {
    try {
      if ($entity = $this->entityRepository->loadEntityByUuid($data['type'], $data['uuid'])) {
        return $entity;
      }
      throw JsonRpcException::fromError(Error::invalidParams('The requested entity could not be found.'));
    }
    catch (EntityStorageException $e) {
      throw JsonRpcException::fromError(Error::invalidParams('This entity type is not supported. Error: ' . $e->getMessage()));
    }
  }

}
