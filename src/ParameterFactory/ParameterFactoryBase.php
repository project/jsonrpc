<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\ParameterFactory;

use Drupal\jsonrpc\ParameterDefinitionInterface;
use Drupal\jsonrpc\ParameterFactoryInterface;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;
use Shaper\Transformation\TransformationBase;
use Shaper\Validator\JsonSchemaValidator;
use Shaper\Validator\ValidateableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for parameter factories.
 */
abstract class ParameterFactoryBase extends TransformationBase implements ParameterFactoryInterface {

  /**
   * The validation class for shaper interactions.
   *
   * @var \Shaper\Validator\ValidateableInterface|null
   */
  protected ?ValidateableInterface $inputValidator = NULL;

  /**
   * Constructs a ParameterFactoryBase object.
   *
   * @param \Drupal\jsonrpc\ParameterDefinitionInterface $definition
   *   The parameter definition.
   * @param \JsonSchema\Validator $validator
   *   The schema validator to ensure the input data adheres to the expectation.
   */
  public function __construct(
    protected ParameterDefinitionInterface $definition,
    protected Validator $validator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ParameterDefinitionInterface $definition, ContainerInterface $container): static {
    return new static($definition, $container->get('jsonrpc.schema_validator'));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getInputValidator(): ?ValidateableInterface {
    if (!$this->inputValidator instanceof ValidateableInterface) {
      $schema = $this->definition->getSchema();
      $this->inputValidator = new JsonSchemaValidator(
        $schema,
        $this->validator,
        Constraint::CHECK_MODE_TYPE_CAST
      );
    }
    return $this->inputValidator;
  }

}
