<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Drush\Generators;

use DrupalCodeGenerator\Asset\AssetCollection as Assets;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\GeneratorType;

/**
 * Generates a JSON-RPC plugin.
 */
#[Generator(
  name: 'jsonrpc:method',
  description: 'Creates a new JSON-RPC method.',
  templatePath: __DIR__,
  type: GeneratorType::MODULE_COMPONENT,
)]
final class JsonRpcMethodGenerator extends BaseGenerator {

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars, Assets $assets): void {
    $ir = $this->createInterviewer($vars);

    $vars['machine_name'] = $ir->askMachineName();
    $vars['name'] = $ir->askName();
    $vars['class'] = $ir->askClass(default: '');
    $vars['id'] = $ir->ask('What should we use for the JSON-RPC ID? (This will be used as the name of the method when calling the JSON-RPC endpoint.', '');
    $vars['description'] = $ir->ask('Description: What does this method do?', '');
    $vars['access'] = $ir->ask('Which permission to set for access control for this method?', 'administer site configuration');

    $assets->addFile('src/Plugin/jsonrpc/Method/{class}.php', 'method.twig');
  }

}
