<?php

declare(strict_types=1);

namespace Drupal\jsonrpc;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Interface to implement a parameter definition.
 */
interface ParameterDefinitionInterface {

  /**
   * The name of the parameter if the params are by-name, an offset otherwise.
   *
   * @return string|int
   *   The ID.
   */
  public function getId(): string|int;

  /**
   * The description of the parameter for the method.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   The description.
   */
  public function getDescription(): ?TranslatableMarkup;

  /**
   * Whether the parameter is required.
   *
   * @return bool
   *   True if this is a required parameter.
   */
  public function isRequired(): bool;

  /**
   * Gets the parameter schema.
   *
   * Can be derived from the type when the schema property is not defined.
   *
   * @return array
   *   The schema.
   */
  public function getSchema(): array;

  /**
   * Get the parameter factory class.
   *
   * @return string
   *   The parameter factory. A default is returned if none has been set.
   */
  public function getFactory(): string;

}
