<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\JsonRpcObject;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Symfony\Component\HttpFoundation\HeaderBag;

/**
 * Response object to help implement the JSON RPC spec for response objects.
 */
class Response implements CacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The JSON-RPC version.
   *
   * @var string
   */
  protected string $version;

  /**
   * A string, number or NULL ID.
   *
   * @var mixed
   */
  protected mixed $id;

  /**
   * The result.
   *
   * @var mixed
   */
  protected mixed $result;

  /**
   * The schema for the result.
   *
   * @var array|null
   */
  protected ?array $resultSchema = NULL;

  /**
   * The error.
   *
   * @var \Drupal\jsonrpc\JsonRpcObject\Error|null
   */
  protected ?Error $error = NULL;

  /**
   * Constructs a JSON-RPC Response object.
   *
   * @param string $version
   *   The JSON-RPC version.
   * @param mixed $id
   *   The response ID. Must match the ID of the generating request.
   * @param mixed $result
   *   A result value. Must not be provided if an error is to be provided.
   * @param \Drupal\jsonrpc\JsonRpcObject\Error|null $error
   *   An error object if the response resulted in an error. Must not be
   *   provided if a result was provided.
   * @param \Symfony\Component\HttpFoundation\HeaderBag $headers
   *   The request headers.
   */
  public function __construct(
    string $version,
    mixed $id,
    mixed $result = NULL,
    ?Error $error = NULL,
    protected HeaderBag $headers = new HeaderBag(),
  ) {
    $this->assertValidResponse($version, $id, $result, $error);
    $this->version = $version;
    $this->id = $id;
    if (!is_null($result)) {
      $this->result = $result;
    }
    else {
      $this->error = $error;
      $this->setCacheability($error);
    }
  }

  /**
   * Gets the ID.
   *
   * @return mixed
   *   The ID.
   */
  public function id(): mixed {
    return $this->id;
  }

  /**
   * Gets the version.
   *
   * @return string
   *   The version.
   */
  public function version(): string {
    return $this->version;
  }

  /**
   * Get the result of the response.
   *
   * @return mixed
   *   The result of the response.
   */
  public function getResult(): mixed {
    return $this->result;
  }

  /**
   * Get the error of the response.
   *
   * @return \Drupal\jsonrpc\JsonRpcObject\Error|null
   *   The error of the response.
   */
  public function getError(): ?Error {
    return $this->error;
  }

  /**
   * Checks if this is an error or result response.
   *
   * @return bool
   *   True if it's a result response.
   */
  public function isResultResponse(): bool {
    return !$this->isErrorResponse();
  }

  /**
   * Checks if this is an error or result response.
   *
   * @return bool
   *   True if it's an error response.
   */
  public function isErrorResponse(): bool {
    return $this->error instanceof Error;
  }

  /**
   * Asserts that the response is valid.
   *
   * @param string $version
   *   The JSON-RPC version.
   * @param mixed $id
   *   The id.
   * @param mixed $result
   *   The result.
   * @param \Drupal\jsonrpc\JsonRpcObject\Error|null $error
   *   The error, if present.
   */
  protected function assertValidResponse(string $version, mixed $id, mixed $result, ?Error $error): void {
    assert(!is_null($result) xor !is_null($error), 'Either the result member or error member MUST be included, but both members MUST NOT be included.');
    assert($version === "2.0", 'A String specifying the version of the JSON-RPC protocol. MUST be exactly "2.0".');
    assert(is_string($id) || is_numeric($id) || is_null($id), 'An identifier established by the Client that MUST contain a String, Number, or NULL value if included.');
  }

  /**
   * The schema of the output response.
   *
   * @return array|null
   *   The result schema.
   */
  public function getResultSchema(): ?array {
    return $this->resultSchema;
  }

  /**
   * Sets the schema for the output response.
   *
   * @param array|null $result_schema
   *   The schema of the result.
   */
  public function setResultSchema(?array $result_schema): void {
    $this->resultSchema = $result_schema;
  }

  /**
   * The headers for this response.
   *
   * @return \Symfony\Component\HttpFoundation\HeaderBag
   *   The header bag object.
   */
  public function getHeaders(): HeaderBag {
    return $this->headers;
  }

}
