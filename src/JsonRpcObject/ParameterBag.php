<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\JsonRpcObject;

/**
 * Value class to hold multiple parameters.
 */
class ParameterBag {

  /**
   * The parameters in the bag.
   *
   * @var array
   *   The parameters.
   */
  protected array $parameters;

  public function __construct(
    array $parameters,
    protected bool $positional = FALSE,
  ) {
    $this->parameters = $this->positional ? array_values($parameters) : $parameters;
  }

  /**
   * Gets the parameter value by its key.
   *
   * @param string|int $key
   *   The parameter key.
   *
   * @return mixed
   *   The parameter.
   */
  public function get(string|int $key): mixed {
    $this->ensure($key);
    return $this->parameters[$key] ?? NULL;
  }

  /**
   * Checks if the bag has a parameter.
   *
   * @param string|int $key
   *   The parameter key.
   *
   * @return bool
   *   True if the param is present.
   */
  public function has(string|int $key): bool {
    $this->checkKeyIsValid($key);
    return isset($this->parameters[$key]);
  }

  /**
   * Checks if the parameter bag is empty.
   *
   * @return bool
   *   True if the bag is empty.
   */
  public function isEmpty(): bool {
    return $this->parameters === [];
  }

  /**
   * Throw an exception if the bag does not have the parameter.
   *
   * @param string|int $key
   *   The parameter key.
   *
   * @throws \InvalidArgumentException
   *   When the parameter is not present in the bag.
   */
  protected function ensure(string|int $key): void {
    $this->checkKeyIsValid($key);
  }

  /**
   * Checks if the key is valid.
   *
   * @param string|int $key
   *   The parameter key.
   *
   * @throws \InvalidArgumentException
   *   If the key is not valid.
   */
  protected function checkKeyIsValid(string|int $key): void {
    if ($this->positional && !is_int($key) && $key >= 0) {
      throw new \InvalidArgumentException('The parameters are by-position. Integer key required.');
    }
    elseif (!is_string($key)) {
      throw new \InvalidArgumentException('The parameters are by-name. String key required.');
    }
  }

}
