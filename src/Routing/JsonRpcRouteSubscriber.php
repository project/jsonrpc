<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\jsonrpc\Enum\JsonRpcSetting;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Allows access to JSON-RPC routes based on the user-defined config.
 */
class JsonRpcRouteSubscriber extends RouteSubscriberBase {

  /**
   * The config defined by the user on the module settings page.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Creates a new JsonRpcRouteSubscriber instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('jsonrpc.settings');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function alterRoutes(RouteCollection $collection): void {
    if (($route = $collection->get('jsonrpc.handler')) instanceof Route) {
      $allowed_authorization_methods = [];

      $basic_auth = boolval($this->config->get(JsonRpcSetting::BasicAuth->value));
      if ($basic_auth) {
        $allowed_authorization_methods[] = JsonRpcSetting::BasicAuth->value;
      }

      $cookie = boolval($this->config->get(JsonRpcSetting::Cookie->value));
      if ($cookie) {
        $allowed_authorization_methods[] = JsonRpcSetting::Cookie->value;
        $route->setRequirement('_csrf_request_header_token', 'TRUE');
      }

      $jwt = boolval($this->config->get(JsonRpcSetting::JWT->value));
      if ($jwt) {
        $allowed_authorization_methods[] = JsonRpcSetting::JWT->value;
      }

      $oauth = boolval($this->config->get(JsonRpcSetting::OAuth2->value));
      if ($oauth) {
        $allowed_authorization_methods[] = JsonRpcSetting::OAuth2->value;
      }

      $route->setOption('_auth', $allowed_authorization_methods);
    }
  }

}
