<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Routing;

use Drupal\Core\EventSubscriber\OptionsRequestSubscriber;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Handles options requests.
 */
class JsonRpcOptionsRequestSubscriber implements EventSubscriberInterface {

  public function __construct(
    protected RouteProviderInterface $routeProvider,
    protected OptionsRequestSubscriber $subject,
  ) {}

  /**
   * Tries to handle the options request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function onRequest(RequestEvent $event): void {
    $request = $event->getRequest();
    if (!$request->isMethod('OPTIONS') || $request->headers->get('access-control-request-method') === 'POST') {
      return;
    }
    $routes = $this->routeProvider->getRouteCollectionForRequest($request);
    // If all routes are for JSON-RPC let the module handle them.
    $all_jsonrpc = array_reduce(
      array_keys($routes->all()),
      fn(bool $carry, string $route_name): bool => $carry && $route_name === 'jsonrpc.handler',
      TRUE
    );
    if (!$all_jsonrpc) {
      $this->subject->onRequest($event);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function getSubscribedEvents(): array {
    return OptionsRequestSubscriber::getSubscribedEvents();
  }

}
