<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Enum;

/**
 * Settings values for the JSON-RPC module.
 */
enum JsonRpcSetting: string {
  case BasicAuth = 'basic_auth';
  case Cookie = 'cookie';
  case JWT = 'jwt_auth';
  case OAuth2 = 'oauth2';
}
