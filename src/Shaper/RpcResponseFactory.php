<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Shaper;

use Drupal\Component\Serialization\Json;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\JsonRpcObject\Response;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;
use Shaper\Transformation\TransformationBase;
use Shaper\Util\Context;
use Shaper\Validator\AcceptValidator;
use Shaper\Validator\CollectionOfValidators;
use Shaper\Validator\InstanceofValidator;
use Shaper\Validator\JsonSchemaValidator;
use Shaper\Validator\ValidateableInterface;

/**
 * Creates RPC Response objects.
 */
class RpcResponseFactory extends TransformationBase {

  const RESPONSE_VERSION_KEY = RpcRequestFactory::REQUEST_VERSION_KEY;
  const REQUEST_IS_BATCH_REQUEST = RpcRequestFactory::REQUEST_IS_BATCH_REQUEST;

  /**
   * The output validator, based on the JSON Schema.
   *
   * @var \Shaper\Validator\ValidateableInterface
   */
  protected ValidateableInterface $outputValidator;

  public function __construct(
    protected Validator $validator,
  ) {}

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getInputValidator(): CollectionOfValidators|ValidateableInterface {
    return new CollectionOfValidators(new InstanceofValidator(Response::class));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getOutputValidator(): AcceptValidator|ValidateableInterface {
    return $this->outputValidator ?: new AcceptValidator();
  }

  /**
   * Sets the schema for the response output.
   *
   * @param array|null $result_schema
   *   The array of the response.
   */
  public function setOutputSchema(?array $result_schema): void {
    $schema = Json::decode(file_get_contents(__DIR__ . '/response-schema.json'));
    $schema['properties']['result'] = $result_schema;
    $this->outputValidator = new JsonSchemaValidator(
      $schema,
      $this->validator,
      Constraint::CHECK_MODE_TYPE_CAST
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function doTransform($data, Context $context) {
    $this->setOutputSchema($data[0]->getResultSchema());
    $output = array_map(function (Response $response) use ($context): array|JsonRpcException {
      try {
        return $this->doNormalize($response, $context);
      }
      catch (\Exception $e) {
        return JsonRpcException::fromPrevious($e, $response->id(), $context[static::RESPONSE_VERSION_KEY]);
      }
    }, $data);
    return $context[static::REQUEST_IS_BATCH_REQUEST] ? $output : reset($output);
  }

  /**
   * Performs the actual normalization.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Response $response
   *   The RPC Response object to return.
   * @param \Shaper\Util\Context $context
   *   The context object.
   *
   * @return array
   *   The normalized response.
   */
  protected function doNormalize(Response $response, Context $context): array {
    $normalized = [
      'jsonrpc' => $context[static::RESPONSE_VERSION_KEY],
      'id' => $response->id(),
    ];
    if ($response->isResultResponse()) {
      $normalized['result'] = $response->getResult();
    }
    if ($response->isErrorResponse()) {
      $error = $response->getError();
      $normalized['error'] = [
        'code' => $error->getCode(),
        'message' => $error->getMessage(),
        'data' => $error->getData(),
      ];
    }
    return $normalized;
  }

}
