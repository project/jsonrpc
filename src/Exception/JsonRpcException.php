<?php

declare(strict_types=1);

namespace Drupal\jsonrpc\Exception;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\jsonrpc\JsonRpcObject\Error;
use Drupal\jsonrpc\JsonRpcObject\Response;

/**
 * Custom exception class for the module.
 */
class JsonRpcException extends \Exception implements CacheableDependencyInterface {

  use CacheableDependencyTrait;

  /**
   * Constructs a JsonRpcException object.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Response $response
   *   The JSON-RPC error response object for the exception.
   * @param \Throwable|null $previous
   *   The previous exception.
   */
  public function __construct(
    protected Response $response,
    ?\Throwable $previous = NULL,
  ) {
    $error = $response->getError();
    $this->setCacheability($response);
    parent::__construct($error->getMessage(), $error->getCode(), $previous);
  }

  /**
   * The appropriate JSON-RPC error response for the exception.
   *
   * @return \Drupal\jsonrpc\JsonRpcObject\Response
   *   The RPC response object.
   */
  public function getResponse(): Response {
    return $this->response;
  }

  /**
   * Constructs a JsonRpcException from an arbitrary exception.
   *
   * @param \Throwable $previous
   *   An arbitrary exception.
   * @param mixed $id
   *   The request ID, if available.
   * @param string|null $version
   *   (optional) The JSON-RPC version.
   *
   * @return static
   */
  public static function fromPrevious(\Throwable $previous, mixed $id = FALSE, ?string $version = NULL): static {
    if ($previous instanceof JsonRpcException) {
      // Ensures that the ID and version context information are set because it
      // might not have been set or accessible at a lower level.
      $response = $previous->getResponse();
      return static::fromError($response->getError(), $response->id() ?: $id, $response->version());
    }
    $error_level = \Drupal::configFactory()->get('system.logging')->get('error_level') ?? ERROR_REPORTING_HIDE;
    $error = Error::internalError($error_level !== ERROR_REPORTING_HIDE ? $previous->getMessage() : NULL);
    $response = static::buildResponse($error, $id, $version);
    return new static($response, $previous);
  }

  /**
   * Constructs a JsonRpcException from an arbitrary error object.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Error $error
   *   The error which caused the exception.
   * @param mixed $id
   *   The request ID, if available.
   * @param string|null $version
   *   (optional) The JSON-RPC version.
   *
   * @return static
   */
  public static function fromError(Error $error, mixed $id = FALSE, ?string $version = NULL): static {
    return new static(static::buildResponse($error, $id, $version));
  }

  /**
   * Helper to build a JSON-RPC response object.
   *
   * @param \Drupal\jsonrpc\JsonRpcObject\Error $error
   *   The error object.
   * @param mixed $id
   *   The request ID.
   * @param string|null $version
   *   The information version.
   *
   * @return \Drupal\jsonrpc\JsonRpcObject\Response
   *   The RPC response object.
   */
  protected static function buildResponse(Error $error, mixed $id = FALSE, ?string $version = NULL): Response {
    $supported_version = $version ?: \Drupal::service('jsonrpc.handler')::supportedVersion();
    return new Response($supported_version, $id ?: NULL, NULL, $error);
  }

}
