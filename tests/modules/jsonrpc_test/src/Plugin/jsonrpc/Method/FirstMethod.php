<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_test\Plugin\jsonrpc\Method;

use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;

/**
 * First test method.
 *
 * @JsonRpcMethod(
 *   id = "first.test",
 *   usage = @Translation("First test method."),
 *   access = {"access content"},
 *   responseHeaders = {
 *     "foo": "bar",
 *     "lorem": "ipsum",
 *     "hello": "world",
 *   }
 * )
 */
class FirstMethod extends JsonRpcMethodBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): int {
    return mt_rand(0, 100);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return [
      'type' => 'number',
    ];
  }

}
