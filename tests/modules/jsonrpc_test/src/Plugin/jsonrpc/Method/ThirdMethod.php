<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_test\Plugin\jsonrpc\Method;

use Drupal\jsonrpc\Handler;
use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\JsonRpcObject\Response;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\HttpFoundation\HeaderBag;

/**
 * Third test method.
 *
 * @JsonRpcMethod(
 *   id = "third.test",
 *   usage = @Translation("Third test method."),
 *   access = {"access content"},
 * )
 */
class ThirdMethod extends JsonRpcMethodBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): Response {
    return new Response(
      Handler::SUPPORTED_VERSION,
      $this->currentRequest()->id(),
      'invalid',
      NULL,
      new HeaderBag(['foo' => 'oof', 'hello' => NULL, 'bye' => 'bye!'])
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return [
      // Schema is invalid intentionally.
      'type' => 'number',
    ];
  }

}
