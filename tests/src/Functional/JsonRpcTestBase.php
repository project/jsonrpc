<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonrpc\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\jsonrpc\Enum\JsonRpcSetting;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Tests POST and GET for requests.
 *
 * @package Drupal\jsonrpc\Tests\Functional
 */
abstract class JsonRpcTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function setUp(): void {
    parent::setUp();

    // Grant authenticated users permission to use JSON-RPC.
    $auth_role = Role::load(RoleInterface::AUTHENTICATED_ID);
    $this->grantPermissions($auth_role, ['use jsonrpc services']);

    $this->user = $this->drupalCreateUser([], 'user', FALSE, ['mail' => 'user@example.com']);

    $this->adminUser = $this->drupalCreateUser([], 'adminUser', TRUE, ['mail' => 'admin@example.com']);
  }

  /**
   * Post a request in JSON format.
   *
   * @param array $rpc_request
   *   The request structure that will be sent as JSON.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user to be used for authentication.
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting|null $auth_method
   *   The auth method to use to authenticate. Required if account is specified.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function postRpc(array $rpc_request, ?AccountInterface $account = NULL, ?JsonRpcSetting $auth_method = NULL) {
    $url = $this->buildUrl(Url::fromRoute('jsonrpc.handler'));
    $request_options = [
      RequestOptions::HTTP_ERRORS => FALSE,
      RequestOptions::ALLOW_REDIRECTS => FALSE,
      RequestOptions::JSON => $rpc_request,
    ];

    $this->addAuthenticationRequestOptions($request_options, $account, $auth_method);

    $client = $this->getHttpClient();
    return $client->request('POST', $url, $request_options);
  }

  /**
   * JSON RPC request using the GET method.
   *
   * @param array $rpc_request
   *   The request structure that will be sent as JSON.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user to be used for authentication.
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting|null $auth_method
   *   The auth method to use to authenticate. Required if account is provided.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getRpc(array $rpc_request, ?AccountInterface $account = NULL, ?JsonRpcSetting $auth_method = NULL): ResponseInterface {
    $url = $this->buildUrl(Url::fromRoute('jsonrpc.handler'));
    $request_options = [
      RequestOptions::HTTP_ERRORS => FALSE,
      RequestOptions::ALLOW_REDIRECTS => FALSE,
      RequestOptions::QUERY => ['query' => Json::encode($rpc_request)],
    ];

    $this->addAuthenticationRequestOptions($request_options, $account, $auth_method);

    $client = $this->getHttpClient();
    return $client->request('GET', $url, $request_options);
  }

  /**
   * Enables a JSON-RPC auth method in config.
   *
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting $auth_method
   *   The authorization method to enable.
   */
  protected function enableAuthMethod(JsonRpcSetting $auth_method): void {
    $this->setAuthMethodStatus($auth_method, TRUE);
  }

  /**
   * Disables a JSON-RPC auth method in config.
   *
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting $auth_method
   *   The authorization method to disable.
   */
  protected function disableAuthMethod(JsonRpcSetting $auth_method): void {
    $this->setAuthMethodStatus($auth_method, FALSE);
  }

  /**
   * Enables or disables a JSON-RPC auth method in config.
   *
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting $auth_method
   *   The authorization method to enable/disable.
   * @param bool $enabled
   *   Whether to enable or disable the authorization method.
   */
  private function setAuthMethodStatus(JsonRpcSetting $auth_method, bool $enabled): void {
    $this->container->get('config.factory')
      ->getEditable('jsonrpc.settings')
      ->set($auth_method->value, $enabled)
      ->save(TRUE);
    // When we change the auth config, the RouteSubscriber must be rebuilt.
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Enables basic_auth for JSON-RPC.
   */
  protected function enableBasicAuth(): void {
    $this->enableAuthMethod(JsonRpcSetting::BasicAuth);
  }

  /**
   * Disables basic_auth for JSON-RPC.
   */
  protected function disableBasicAuth(): void {
    $this->disableAuthMethod(JsonRpcSetting::BasicAuth);
  }

  /**
   * Enables cookie auth for JSON-RPC.
   */
  protected function enableCookieAuth(): void {
    $this->enableAuthMethod(JsonRpcSetting::Cookie);
  }

  /**
   * Disables cookie auth for JSON-RPC.
   */
  protected function disableCookieAuth(): void {
    $this->disableAuthMethod(JsonRpcSetting::Cookie);
  }

  /**
   * Adds the authentication-related request options.
   *
   * @param array $request_options
   *   The request options to use for JSON-RPC.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user to be used for authentication.
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting|null $auth_method
   *   The auth method to use to authenticate. Required if account is provided.
   *
   * @return void
   *   The $request_options array is modified in place, so no value is returned.
   */
  private function addAuthenticationRequestOptions(array &$request_options, ?AccountInterface $account = NULL, ?JsonRpcSetting $auth_method = NULL): void {
    if ($account instanceof AccountInterface) {
      if (is_null($auth_method)) {
        throw new \Exception("If account is specified, auth_method must also be specified.");
      }
      if ($auth_method === JsonRpcSetting::BasicAuth) {
        $request_options[RequestOptions::AUTH] = [
          $account->getAccountName(),
          $account->passRaw,
        ];
      }
      elseif ($auth_method === JsonRpcSetting::Cookie) {
        // For cookies, we need to log in get the cookie.
        $this->drupalLogin($account);

        $request_options[RequestOptions::COOKIES] = $this->getSessionCookies();

        // Get the CSRF token and set the header.
        $csrf_token_url = Url::fromRoute('system.csrftoken')
          ->setAbsolute()->toString();

        // Make sure the token is for the user account.
        $csrf_token_response = \Drupal::httpClient()->get($csrf_token_url, [
          RequestOptions::COOKIES => $this->getSessionCookies(),
          RequestOptions::HEADERS => [],
          RequestOptions::BODY => NULL,
        ]);

        // See CookieResourceTestTrait.
        $csrf_token = (string) $csrf_token_response->getBody();
        $this->assertEquals(43, strlen($csrf_token), "CSRF token seems invalid! It should be 43 characters. $csrf_token");
        $request_options[RequestOptions::HEADERS]['X-CSRF-Token'] = $csrf_token;
      }
    }
  }

}
