<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonrpc\Functional;

use Drupal\Tests\jsonrpc_discovery\Functional\JsonRpcDiscoveryFunctionalTestBase;
use Drupal\user\UserInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Tests the configuration for cookie auth.
 *
 * @group jsonrpc
 */
class JsonRpcDiscoveryCookieAuthTest extends JsonRpcDiscoveryFunctionalTestBase {

  /**
   * Sends a GET request to a JSON-RPC method.
   *
   * @param string $method_url
   *   The URL of the JSON-RPC method.
   * @param \Drupal\user\UserInterface|null $user
   *   The user interface of the user account. For anon, use null.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the JSON-RPC method.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  #[\Override]
  protected function getJsonRpcMethod(string $method_url, ?UserInterface $user = NULL): ResponseInterface {
    $request_options = [
      RequestOptions::BODY => NULL,
      RequestOptions::HEADERS => [],
    ];
    if ($user instanceof UserInterface) {
      $request_options[RequestOptions::COOKIES] = $this->getSessionCookies();
    }
    return \Drupal::httpClient()->get($method_url, $request_options);
  }

  /**
   * Tests getting the methods as an anonymous user with cookie auth.
   */
  public function testCookieAnon(): void {
    $this->enableCookieAuth();
    // Anon does not have access to JSON-RPC services.
    $method_url = $this->getMethodsUrl();
    try {
      $this->getJsonRpcMethod($method_url);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('401 Unauthorized', $e->getMessage());
    }
  }

  /**
   * Tests getting the methods as an auth user with cookie auth.
   */
  public function testCookieAuth(): void {
    $this->enableCookieAuth();
    $this->drupalLogin($this->user);
    $method_url = $this->getMethodsUrl();
    $auth_response = $this->getJsonRpcMethod($method_url, $this->user);
    $this->assertEquals(200, $auth_response->getStatusCode());

    $this->disableCookieAuth();
    try {
      $this->getJsonRpcMethod($method_url, $this->user);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('403', $e->getMessage());
    }
  }

  /**
   * Tests getting the methods as an admin user with cookie auth.
   */
  public function testCookieAdmin(): void {
    $this->enableCookieAuth();
    $this->drupalLogin($this->adminUser);
    $method_url = $this->getMethodsUrl();
    $admin_response = $this->getJsonRpcMethod($method_url, $this->adminUser);
    $this->assertEquals(200, $admin_response->getStatusCode());

    $this->disableCookieAuth();
    try {
      $this->getJsonRpcMethod($method_url, $this->adminUser);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('403', $e->getMessage());
    }
  }

}
