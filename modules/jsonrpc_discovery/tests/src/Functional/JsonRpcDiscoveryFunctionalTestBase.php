<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonrpc_discovery\Functional;

use Drupal\Core\Url;
use Drupal\Tests\jsonrpc\Functional\JsonRpcTestBase;
use Drupal\user\UserInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * This class provides methods specifically for testing something.
 *
 * @group jsonrpc
 */
abstract class JsonRpcDiscoveryFunctionalTestBase extends JsonRpcTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'basic_auth',
    'jsonrpc',
    'jsonrpc_core',
    'jsonrpc_discovery',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Executes a request to jsonrpc/methods.
   *
   * @return string
   *   The absolute url.
   */
  protected function getMethodsUrl(): string {
    return Url::fromRoute('jsonrpc.method_collection')
      ->setAbsolute()->toString();
  }

  /**
   * Provides a basic auth header.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account.
   *
   * @return string
   *   The basic auth header value formatted for Guzzle.
   */
  private function getAuthForUser(UserInterface $user): string {
    $name = $user->getAccountName();
    $pass = $user->passRaw;
    return 'Basic ' . base64_encode($name . ':' . $pass);
  }

  /**
   * Sends a GET request to a JSON-RPC method.
   *
   * @param string $method_url
   *   The URL of the JSON-RPC method.
   * @param \Drupal\user\UserInterface|null $user
   *   The user interface of the user account. For anon, use null.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the JSON-RPC method.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getJsonRpcMethod(string $method_url, ?UserInterface $user = NULL): ResponseInterface {
    $request_options = [
      RequestOptions::BODY => NULL,
      RequestOptions::HEADERS => [],
    ];

    if ($user instanceof UserInterface) {
      $request_options[RequestOptions::HEADERS]['Authorization'] = $this->getAuthForUser($user);
    }
    return \Drupal::httpClient()->get($method_url, $request_options);
  }

}
