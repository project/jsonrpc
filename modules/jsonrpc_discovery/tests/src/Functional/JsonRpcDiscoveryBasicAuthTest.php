<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonrpc\Functional;

use Drupal\Tests\jsonrpc_discovery\Functional\JsonRpcDiscoveryFunctionalTestBase;

/**
 * Tests the configuration for basic_auth.
 *
 * @group jsonrpc
 */
class JsonRpcDiscoveryBasicAuthTest extends JsonRpcDiscoveryFunctionalTestBase {

  /**
   * Tests getting the methods as an anonymous user with basic auth.
   */
  public function testBasicAuthAnon(): void {
    $this->enableBasicAuth();
    // Anon does not have access to JSON-RPC services.
    $method_url = $this->getMethodsUrl();

    try {
      $this->getJsonRpcMethod($method_url);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('401 Unauthorized', $e->getMessage());
    }
  }

  /**
   * Tests getting the methods as an auth user with basic auth.
   */
  public function testBasicAuthAuth(): void {
    $this->enableBasicAuth();
    $method_url = $this->getMethodsUrl();
    $auth_response = $this->getJsonRpcMethod($method_url, $this->user);
    $this->assertEquals(200, $auth_response->getStatusCode());

    $this->disableBasicAuth();
    try {
      $this->getJsonRpcMethod($method_url, $this->user);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('403', $e->getMessage());
    }
  }

  /**
   * Tests getting the methods as an admin user with basic auth.
   */
  public function testBasicAuthAdmin(): void {
    $this->enableBasicAuth();
    $method_url = $this->getMethodsUrl();
    $admin_response = $this->getJsonRpcMethod($method_url, $this->adminUser);
    $this->assertEquals(200, $admin_response->getStatusCode());

    $this->disableBasicAuth();
    try {
      $this->getJsonRpcMethod($method_url, $this->adminUser);
    }
    catch (\Exception $e) {
      $this->assertStringContainsString('403', $e->getMessage());
    }
  }

}
