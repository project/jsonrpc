<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_core\Plugin\jsonrpc\Method;

use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base method for methods that ned access to the user.permissions service.
 */
abstract class UserPermissionsBase extends JsonRpcMethodBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected PermissionHandlerInterface $permissions,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('user.permissions')
    );
  }

}
