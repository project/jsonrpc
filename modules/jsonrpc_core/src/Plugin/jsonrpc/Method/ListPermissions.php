<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_core\Plugin\jsonrpc\Method;

use Drupal\jsonrpc\JsonRpcObject\ParameterBag;

/**
 * RPC method to list all the permissions.
 *
 * @JsonRpcMethod(
 *   id = "user_permissions.list",
 *   usage = @Translation("List all the permissions available in the site."),
 *   access = {"administer permissions"},
 *   params = {
 *     "page" = @JsonRpcParameterDefinition(factory = "\Drupal\jsonrpc\ParameterFactory\PaginationParameterFactory"),
 *   }
 * )
 */
class ListPermissions extends UserPermissionsBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): array {
    $page = $params->get('page');
    return array_slice(
      $this->permissions->getPermissions(),
      $page['offset'],
      $page['limit']
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    // @todo Fix the schema.
    return ['type' => 'foo'];
  }

}
