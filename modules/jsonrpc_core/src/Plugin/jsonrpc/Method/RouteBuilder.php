<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_core\Plugin\jsonrpc\Method;

use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * RPC method to rebuild the routes.
 *
 * @JsonRpcMethod(
 *   id = "route_builder.rebuild",
 *   access = {"administer site configuration"},
 *   usage = @Translation("Rebuilds the application's router. Result is TRUE if the rebuild succeeded, FALSE otherwise"),
 * )
 */
class RouteBuilder extends JsonRpcMethodBase {

  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected RouteBuilderInterface $routeBuilder,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): bool {
    return $this->routeBuilder->rebuild();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): null {
    return NULL;
  }

}
