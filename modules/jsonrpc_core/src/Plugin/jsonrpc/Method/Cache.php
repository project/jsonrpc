<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_core\Plugin\jsonrpc\Method;

use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;

/**
 * Rebuilds all Drupal caches.
 *
 * @JsonRpcMethod(
 *   id = "cache.rebuild",
 *   usage = @Translation("Rebuilds the system cache."),
 *   access = {"administer site configuration"},
 * ),
 */
class Cache extends JsonRpcMethodBase {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): true {
    drupal_flush_all_caches();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): null {
    return NULL;
  }

}
