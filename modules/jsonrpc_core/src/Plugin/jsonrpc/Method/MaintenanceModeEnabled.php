<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_core\Plugin\jsonrpc\Method;

use Drupal\Core\State\StateInterface;
use Drupal\jsonrpc\JsonRpcObject\ParameterBag;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * RPC method to enable or disable maintenance mode.
 *
 * @JsonRpcMethod(
 *   id = "maintenance_mode.isEnabled",
 *   usage = @Translation("Enables or disables the maintenance mode."),
 *   access = {"administer site configuration"},
 *   params = {
 *     "enabled" = @JsonRpcParameterDefinition(schema={"type"="boolean"}),
 *   }
 * ),
 */
class MaintenanceModeEnabled extends JsonRpcMethodBase {

  const ENABLED = 'enabled';
  const DISABLED = 'disabled';

  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected StateInterface $state,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('state'));
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): string {
    $enabled = $params->get('enabled');

    $this->state->set('system.maintenance_mode', $enabled);
    return $this->state->get('system.maintenance_mode')
      ? static::ENABLED
      : static::DISABLED;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return ['type' => 'string'];
  }

}
