<?php

declare(strict_types=1);

namespace Drupal\Tests\jsonrpc_core\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Tests\jsonrpc\Functional\JsonRpcTestBase;
use Drupal\jsonrpc\Enum\JsonRpcSetting;
use GuzzleHttp\RequestOptions;

/**
 * Test turning the maintenance mode on or off using JSON-RPC.
 *
 * @group jsonrpc
 */
class MaintenanceModeEnabledTest extends JsonRpcTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jsonrpc',
    'jsonrpc_core',
    'basic_auth',
    'serialization',
  ];

  /**
   * The POST request to enable maintenance mode.
   *
   * @var array
   */
  protected array $enableRequest = [
    'jsonrpc' => '2.0',
    'method' => 'maintenance_mode.isEnabled',
    'params' => [
      'enabled' => TRUE,
    ],
    'id' => 'maintenance_mode_enabled',
  ];

  /**
   * The POST request to disabled maintenance mode.
   *
   * @var array
   */
  protected array $disableRequest = [
    'jsonrpc' => '2.0',
    'method' => 'maintenance_mode.isEnabled',
    'params' => [
      'enabled' => FALSE,
    ],
    'id' => 'maintenance_mode_disabled',
  ];

  /**
   * Tests enabling maintenance mode.
   *
   * @param \Drupal\jsonrpc\Enum\JsonRpcSetting $auth_method
   *   The auth method to use for the test.
   */
  private function enableMaintenanceModeTestFlow(JsonRpcSetting $auth_method): void {
    // Assert that anonymous users are not able to enable the maintenance page.
    $anon_response = $this->postRpc($this->enableRequest);
    $this->assertSame(401, $anon_response->getStatusCode());

    // Retry request with auth.
    $auth_response = $this->postRpc($this->enableRequest, $this->adminUser, $auth_method);
    $this->assertSame(200, $auth_response->getStatusCode());
    $parsed_body = Json::decode($auth_response->getBody());
    $expected = [
      'jsonrpc' => '2.0',
      'id' => 'maintenance_mode_enabled',
      'result' => 'enabled',
    ];
    $this->assertEquals($expected, $parsed_body);

    // Assert maintenance mode is enabled.
    // If using cookie auth, we need to log out first because we are an admin.
    if ($auth_method === JsonRpcSetting::Cookie) {
      $this->drupalLogout();
    }
    $this->drupalGet('/jsonrpc');
    $this->assertEquals('Site under maintenance', $this->cssSelect('main h1')[0]->getText());

    // Send request to disable maintenance mode.
    $disabled_response = $this->postRpc($this->disableRequest, $this->adminUser, $auth_method);
    $this->assertSame(200, $disabled_response->getStatusCode());
    $parsed_body = Json::decode($disabled_response->getBody());
    $expected = [
      'jsonrpc' => '2.0',
      'id' => 'maintenance_mode_disabled',
      'result' => 'disabled',
    ];
    $this->assertEquals($expected, $parsed_body);

    // Assert maintenance mode is disabled.
    // If using cookie auth, we need to log out first because we are an admin.
    if ($auth_method === JsonRpcSetting::Cookie) {
      $this->drupalLogout();
    }
    $this->drupalGet('/jsonrpc');
    $this->assertNotEquals('Site under maintenance', $this->cssSelect('main h1')[0]->getText());
  }

  /**
   * Tests enabling maintenance mode with basic auth.
   */
  public function testEnablingMaintenanceModeBasicAuth(): void {
    $this->enableBasicAuth();
    $this->enableMaintenanceModeTestFlow(JsonRpcSetting::BasicAuth);
  }

  /**
   * Tests enabling maintenance mode with cookie auth.
   */
  public function testEnablingMaintenanceModeCookie(): void {
    $this->enableCookieAuth();
    $this->enableMaintenanceModeTestFlow(JsonRpcSetting::Cookie);
  }

  /**
   * Tests that enabling maintenance mode by cookie requires a CSRF token.
   */
  public function testEnablingMaintenanceModeCookieNoCsrfToken(): void {
    $this->enableCookieAuth();

    $url = $this->buildUrl(Url::fromRoute('jsonrpc.handler'));
    $request_options = [
      RequestOptions::HTTP_ERRORS => FALSE,
      RequestOptions::ALLOW_REDIRECTS => FALSE,
      RequestOptions::JSON => $this->enableRequest,
    ];

    // For cookies, we need to log in get the cookie.
    $this->drupalLogin($this->adminUser);
    // Include cookies, but do not include a CSRF token.
    $request_options[RequestOptions::COOKIES] = $this->getSessionCookies();

    $client = $this->getHttpClient();
    $no_csrf_token_response = $client->request('POST', $url, $request_options);

    $this->assertSame(403, $no_csrf_token_response->getStatusCode());
  }

}
